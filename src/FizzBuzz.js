import React, { Component } from "react";
import PropTypes from "prop-types";

class FizzBuzz extends Component {
  constructor(props) {
    super(props);

    this.state = {
      listNumber: [],
      errors: {},
      days: [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
      ]
    };
  }

  setStateToDefault(event) {
    var tempArr = [];
    var today = new Date().getDay();
    var dayName = this.state.days[today];

    if (event.target.value <= 1000) {
      this.state.errors["msg"] = " ";

      for (let i = 1; i <= event.target.value; i++) {
        if (i % 3 === 0 && i % 5 === 0) {
          tempArr.push("fizz buzz");
        } else if (i % 3 === 0) {
          tempArr.push(<h1 style={{ color: "blue" }}>{dayName[0]}izz</h1>);
        } else if (i % 5 === 0) {
          tempArr.push(<h1 style={{ color: "green" }}>{dayName[0]}uzz</h1>);
        } else {
          tempArr.push(i);
        }
      }
    } else {
      this.state.errors["msg"] = "Number should less than 1000";
    }

    this.setState(
      {
        listNumber: []
      },
      function() {
        this.assignListNumberByTempArray(tempArr);
      }
    );
  }

  assignListNumberByTempArray(tempArr) {
    this.setState({
      listNumber: Object.assign(this.state.listNumber, tempArr)
    });
  }

  render() {
    return (
      <>
        <input
          type="number"
          onChange={event => this.setStateToDefault(event)}
        />
        <ul style={{ listStyleType: "none" }}>
          {this.state.listNumber.map(number => (
            <li>{number}</li>
          ))}
        </ul>
        <span style={{ color: "red" }}>{this.state.errors["msg"]}</span>
      </>
    );
  }
}

export default FizzBuzz;
