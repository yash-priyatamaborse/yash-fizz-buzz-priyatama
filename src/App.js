import React from "react";
import logo from "./logo.svg";
import FizzBuzz from "./FizzBuzz";
import "./App.css";

function App() {
  return (
    <>
      <FizzBuzz></FizzBuzz>
    </>
  );
}

export default App;
